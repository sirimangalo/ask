apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    {{ include "common.labels" . | nindent 4 }}
    service: {{ .Values.web.name }}-database
  name: {{ .Values.web.name }}-database
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      {{/* Commented out because label selctor updates require the deployment to be deleted and recreated. */}}
      {{/* Would be nice to add matching the common labels in the future, but it's not strictly required. */}}
      {{/* include "common.labels" . | nindent 6 */}}
      service: {{ .Values.web.name }}-database
  template:
    metadata:
      labels:
        {{ include "common.labels" . | nindent 8 }}
        service: {{ .Values.web.name }}-database
      annotations:
        prometheus.io/scrape: 'true'
        timestamp: "{{ now | date "20060102150405" }}"
        container.apparmor.security.beta.kubernetes.io/{{ .Values.web.name }}-database: "runtime/default"
        seccomp.security.alpha.kubernetes.io/pod: "runtime/default"
    spec:
      automountServiceAccountToken: false
      {{- with .Values.priorityClassName }}
      priorityClassName: {{ . | trim }}
      {{ end }}
      securityContext:
        fsGroup: 999
      initContainers:
        - name: mariadb-data-permission-fix
          image: busybox
          command: ["/bin/chown", "-R", "999", "/data"]
          volumeMounts:
          - name: mysql-data
            mountPath: /data
      containers:
        - env:
          - name: MYSQL_ROOT_PASSWORD
            valueFrom:
              secretKeyRef:
                key: MYSQL_ROOT_PASSWORD
                name: {{ .Values.web.name }}-secrets
          - name: MYSQL_USER
            valueFrom:
              secretKeyRef:
                key: MYSQL_USER
                name: {{ .Values.web.name }}-secrets
          - name: MYSQL_PASSWORD
            valueFrom:
              secretKeyRef:
                key: MYSQL_PASSWORD
                name: {{ .Values.web.name }}-secrets
          - name: MYSQL_DATABASE
            valueFrom:
              secretKeyRef:
                key: MYSQL_DATABASE
                name: {{ .Values.web.name }}-secrets
          ports:
            - containerPort: 3306
          image: mariadb:10.5
          name: {{ .Values.web.name }}-database
          securityContext:
            runAsNonRoot: true
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
            runAsUser: 999
            runAsGroup: 999
            capabilities:
              drop:
                - all
          resources:
            limits:
              memory: "2Gi"
              cpu: "2"
            requests:
              memory: "120Mi"
              cpu: "5m"
          volumeMounts:
            - name: mysql-data
              mountPath: /var/lib/mysql
            - name: tmp
              mountPath: /tmp
            - name: sock
              mountPath: /run/mysqld
      volumes:
        - name: mysql-data
          {{- if eq .Values.web.environment "Production" }}
          persistentVolumeClaim:
            claimName: {{ .Values.web.name }}-database-pvc
          {{- else }}
          emptyDir: {}
          {{- end }}
        - name: tmp
          emptyDir: {}
        - name: sock
          emptyDir: {}
      restartPolicy: Always
---
{{ if eq .Values.web.environment "Production" }}
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ .Values.web.name }}-database-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
{{ end }}
---
apiVersion: v1
kind: Service
metadata:
  labels:
    {{ include "common.labels" . | nindent 4 }}
  name: {{ .Values.web.name }}-database-service
spec:
  selector:
    {{ include "common.labels" . | nindent 4 }}
    service: {{ .Values.web.name }}-database
  ports:
    - port: 3306
      targetPort: 3306
---
{{ if eq .Values.web.environment "Production" }}
apiVersion: batch/v1
kind: CronJob
metadata:
  labels:
    {{ include "common.labels" . | nindent 4 }}
  name: {{ .Values.web.name }}-backup-cronjob
spec:
  schedule: "0 5 * * *"
  successfulJobsHistoryLimit: 5
  failedJobsHistoryLimit: 10
  jobTemplate:
    spec:
      activeDeadlineSeconds: 600
      backoffLimit: 3
      template:
        metadata:
          labels:
            {{ include "common.labels" . | nindent 12 }}
            networking/allow-database-access: 'true'
            networking/allow-internet-egress: 'true'
        spec:
          automountServiceAccountToken: false
          {{- with .Values.priorityClassNameCronjob }}
          priorityClassName: {{ . | trim }}
          {{ end }}
          restartPolicy: OnFailure
          containers:
          - name: {{ .Values.web.name }}-backup-cronjob
            image: registry.gitlab.com/sirimangalo/docker-images/mariadbdump:10.5-latest
            imagePullPolicy: Always
            env:
              - name: MYSQL_USER
                valueFrom:
                  secretKeyRef:
                    key: MYSQL_USER
                    name: {{ .Values.web.name }}-secrets
              - name: MYSQL_PASSWORD
                valueFrom:
                  secretKeyRef:
                    key: MYSQL_PASSWORD
                    name: {{ .Values.web.name }}-secrets
              - name: MYSQL_DB
                valueFrom:
                  secretKeyRef:
                    key: MYSQL_DATABASE
                    name: {{ .Values.web.name }}-secrets
              - name: MYSQL_PORT
                value: "3306"
              - name: MYSQL_HOST
                value: {{ .Values.web.name }}-database-service
              - name: S3_BUCKET_NAME
                valueFrom:
                  secretKeyRef:
                    key: SPACES_BUCKET_NAME
                    name: {{ .Values.web.name }}-secrets
              - name: S3_ACCESS_KEY
                valueFrom:
                  secretKeyRef:
                    key: SPACES_KEY
                    name: {{ .Values.web.name }}-secrets
              - name: S3_SECRET_KEY
                valueFrom:
                  secretKeyRef:
                    key: SPACES_SECRET
                    name: {{ .Values.web.name }}-secrets
              - name: S3_REGION
                valueFrom:
                  secretKeyRef:
                    key: SPACES_REGION
                    name: {{ .Values.web.name }}-secrets
              - name: S3_HOST
                value: "digitaloceanspaces.com"
              - name: S3_PATH_PREFIX
                valueFrom:
                  secretKeyRef:
                    key: SPACES_PATH_PREFIX
                    name: {{ .Values.web.name }}-secrets
              - name: SUCCESS_WEBHOOK
                valueFrom:
                  secretKeyRef:
                    key: BACKUP_SUCCESS_WEBHOOK
                    name: {{ .Values.web.name }}-secrets
            resources:
              requests:
                memory: "418Mi"
                cpu: "10m"
              limits:
                memory: "418Mi"
                cpu: "500m"
{{ end }}
