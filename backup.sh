#
# Backup ask application
#

# Create backup ID
printf -v datestr '%(%Y%m%d%H%M%S)T' -1; 
export BACKUP_ID=$datestr-$(uuidgen)
echo Creating backup with ID: $BACKUP_ID

# 0. Ensure ~/.my.cnf with the following contents
# 
# [mysqldump]
# user=root
# password=<password>

# 1. Clean up backup folder
rm -f "/home/simc-forums-admin/backup/$BACKUP_ID.tar.gz"
rm -rf "/home/simc-forums-admin/backup/$BACKUP_ID"
mkdir -p "/home/simc-forums-admin/backup/$BACKUP_ID"

# 1. Back up mysql
mysqldump -u root --all-databases > "/home/simc-forums-admin/backup/$BACKUP_ID/all_databases_backup.sql"

# 2. Copy www
cp -r /var/www/html "/home/simc-forums-admin/backup/$BACKUP_ID/"

# 3. Compress
tar -czf "/home/simc-forums-admin/backup/$BACKUP_ID.tar.gz" "/home/simc-forums-admin/backup/$BACKUP_ID"

# 4. Upload to cloud
# Dowload azcopy: wget https://aka.ms/downloadazcopy-v10-linux; tar -xvf downloadazcopy-v10-linux
./azcopy_linux_amd64_10.5.1/azcopy copy "/home/simc-forums-admin/backup/$BACKUP_ID.tar.gz" https://simcforums1.blob.core.windows.net/vmbackups/backups/$BACKUP_ID.tar.gz'<saskey>'
