FROM php:7.4-fpm

ARG GITREF="dev"
ENV DEBIAN_FRONTEND noninteractive

ENV STOPFORUM_SPAM_URL_PARTIAL "https://www.stopforumspam.com/downloads/toxic_domains_partial.txt"
ENV STOPFORUM_SPAM_URL_WHOLE "https://www.stopforumspam.com/downloads/toxic_domains_whole.txt"
ENV DISPOSABLE_DOMAINS_URL "https://raw.githubusercontent.com/ivolo/disposable-email-domains/master/index.json"
ARG Q2A_VERSION="1.8.6"
ENV Q2A_FILE_NAME "question2answer-${Q2A_VERSION}"
ENV Q2A_DOWNLOAD_URL "https://github.com/q2a/question2answer/releases/download/v${Q2A_VERSION}/${Q2A_FILE_NAME}.zip"

# Setup and install base system software
RUN echo "locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8" | debconf-set-selections \
    && echo "locales locales/default_environment_locale select en_US.UTF-8" | debconf-set-selections \
    && apt-get update && apt-get --yes --no-install-recommends install \
        locales ca-certificates curl vim nano unzip tar tzdata \
    && rm -rf /var/lib/apt/lists/*

# Configure php and install extensions
RUN mv "${PHP_INI_DIR}/php.ini-production" "${PHP_INI_DIR}/php.ini" \
    && docker-php-ext-install mysqli \
    && sed -i -e "s/session.cookie_httponly.*/session.cookie_httponly = true/" "${PHP_INI_DIR}/php.ini" \
    && sed -i -e "s/expose_php.*/expose_php = Off/" "${PHP_INI_DIR}/php.ini"

# Get q2a
ENV SRC_DIR /opt/q2a/src/

RUN groupadd -r medask && useradd -r -g medask medask

WORKDIR /opt/q2a/

RUN curl -L ${Q2A_DOWNLOAD_URL} --output ${Q2A_FILE_NAME}.zip \
    && unzip ${Q2A_FILE_NAME}.zip \
    && rm ${Q2A_FILE_NAME}.zip \
    && mv ${Q2A_FILE_NAME} src

COPY custom/qa-theme src/qa-theme
COPY custom/qa-plugin src/qa-plugin

RUN curl -L ${STOPFORUM_SPAM_URL_PARTIAL} --output src/qa-plugin/registration-blocker/banned-hosts-partial.txt
RUN curl -L ${STOPFORUM_SPAM_URL_WHOLE} --output src/qa-plugin/registration-blocker/banned-hosts-whole.txt

# Manually banned hosts
RUN echo "\nmail.ru\nusgeek.org" >> src/qa-plugin/registration-blocker/banned-hosts-whole.txt

RUN curl -L ${DISPOSABLE_DOMAINS_URL} --output src/qa-plugin/registration-blocker/disposable-hosts.json
RUN echo "${GITREF}" > src/version

RUN chgrp -R medask /opt/q2a && \
    chown -R medask /opt/q2a && \
    chmod -R 770 /opt/q2a

COPY docker-entrypoint.sh /usr/local/bin/

RUN chmod +x /usr/local/bin/docker-entrypoint.sh

USER medask

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]