<?php
/*
	Plugin Name: EmailBlocker
	Plugin Description: EmailBlocker Plugin for Question2Answer
	Plugin Version: 1.0
	Plugin Author: Sirimangalo
	Plugin License: MIT
	Plugin Minimum Question2Answer Version: 1.8.0
	Plugin Update Check URI:
*/


if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}

qa_register_plugin_module( 'filter', 'qa-email-blocker.php', 'qas_email_blocker', 'Q2A Email Blocker' );
