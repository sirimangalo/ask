<?php

class qas_email_blocker
{
    public function filter_email(&$email, $olduser)
    {
        $result = $this->filterBannedPartialHosts((string) $email);
        if ($result !== null) {
            return $result;
        }

        $result = $this->filterBannedWholeHosts((string) $email);
        if ($result !== null) {
            return $result;
        }

        $result = $this->filterDisposableHosts((string) $email);
        if ($result !== null) {
            return $result;
        }
    }

    public function filterDisposableHosts(string $email): ?string
    {
        $file = __DIR__ . '/disposable-hosts.json';

        if (!file_exists($file)) {
            return null;
        }

        $disposableHosts = file_get_contents($file);

        if ($disposableHosts === false) {
            return null;
        }

        $disposableHosts = json_decode($disposableHosts, true);

        if ($disposableHosts === null) {
            return null;
        }

        $emailDomain = substr(strrchr($email, '@'), 1);

        if (in_array(strtolower($emailDomain), $disposableHosts)) {
            return 'This email domain is not allowed.';
        }

        return null;
    }

    private function filterBannedPartialHosts(string $email): ?string
    {
        $file = __DIR__ . '/banned-hosts-partial.txt';

        if (!file_exists($file)) {
            return null;
        }

        $bannedHosts = file($file, FILE_IGNORE_NEW_LINES);

        $emailDomain = substr(strrchr($email, '@'), 1);

        foreach ($bannedHosts as $host) {
            if ($this->endsWith(strtolower($emailDomain), $host)) {
                return 'Your email provider is not supported.';
            }
        }

        return null;
    }

    private function filterBannedWholeHosts(string $email): ?string
    {
        $file = __DIR__ . '/banned-hosts-whole.txt';

        if (!file_exists($file)) {
            return null;
        }

        $bannedHosts = file($file, FILE_IGNORE_NEW_LINES);

        $emailDomain = substr(strrchr($email, '@'), 1);

        if (in_array(strtolower($emailDomain), $bannedHosts)) {
            return 'Your email provider is not supported.';
        }

        return null;
    }

    private function endsWith(string $haystack, string $needle): bool
    {
        $length = strlen($needle);
        return $length > 0 ? substr($haystack, -$length) === $needle : true;
    }
}
