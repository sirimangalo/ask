# Helper for local Kubernetes initialization

helm upgrade \
  --install \
  --set web.name="ask-dev" \
  --set web.image="registry.gitlab.com/sirimangalo/ask:feat-k8s" \
  --set web.protocol="http" \
  --wait \
  ask-dev \
  ./k8s/chart
